extends Node3D

@onready var fire = $Fire
@onready var explosion_area = $Area3D
var explosion_effect_pending : bool = false

const impulse_force = 1000;

func explode():
	explosion_effect_pending = true
	fire.emitting = true
	
func _physics_process(_delta):
	if explosion_effect_pending:
		var bodies = explosion_area.get_overlapping_bodies()
		
		for body in bodies:
			if body is RigidBody3D:
				var direction = (body.global_position - global_position).normalized()
				body.apply_central_impulse(direction * impulse_force)
		
		explosion_effect_pending = false
		await get_tree().create_timer(1.0).timeout
		queue_free()
